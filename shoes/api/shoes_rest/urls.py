from django.urls import path
from .views import api_list_shoes, api_show_shoes


urlpatterns = [
    path("shoe/", api_list_shoes, name="api_create_shoes"),
    path(
        "bin/<int:bin_vo_id>/shoe/",
        api_list_shoes,
        name="api_list_shoes",
    ),
    path("shoe/<int:pk>/", api_show_shoes, name="api_show_shoes"),
]
