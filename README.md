# Wardrobify

Team:

* Genessy Munoz - Hats
* Sean Burch - Shoes

## Getting Started

**Make sure you have Docker, Git, and Node.js 18.2 or above**

1. Fork this repository

2. Clone the forked repository onto your local computer:
git clone https://gitlab.com/micah.a.awtrey/microservice-two-shot

3. Build and run the project using Docker with these commands:
```
docker volume create two-shot-pgdata
docker-compose build
docker-compose up
```
- After running these commands, make sure all of your Docker containers are running

- View the project in the browser: http://localhost:3000/

![Img](HatShoe_DDD_Diagram.png)

## Design

HatShoe is comprised of three microservices:

- **Wardrobe**
- **Hats**
- **Shoes**

## Integration - How we put the "team" in "team"

Team Genessy and Team Sean worked hard to create systems that work entirely independently to create their own respective clothing items. Team Genessey used their masterful skills to create the **Hats** microservice, allowing the creation and management of hats. Team Sean astounded with their exceptional **Shoes** microservice, allowing the creation and management of shoes.

In order to ensure that each microservice could function without interrupting the other, the **Hats** and **Shoes** microservices each used their own **poller** to get data from the **Wardrobe** microservice. These pollers ensure that all data is kept up to date for all facets of this project.

## Accessing Endpoints to Send and View Data: Access Through Insomnia & Your Browser

### Locations:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List Locations | GET | http://localhost:8100/api/locations/
| Create a Location | POST | http://localhost:8100/api/locations/ |
| Get a specific Location's Details | GET | http://localhost:8100/api/locations/id/ |
| Update a specific Location | PUT | http://localhost:8100/api/locations/id/ |
| Delete a specific Location| DELETE | http://localhost:8100/api/locations/id/ |

JSON body to send data:

Create and Update a Location (SEND THIS JSON BODY):
```
{
  "name": "The Dark Lord's Closet"
  "section_number": 123,
  "shelf_number": 12
}
```
The return value of creating, viewing, updating a single Location:
```
{
	"href": "/api/locations/2/",
	"id": 2,
	"closet_name": "The Dark Lord's Closet",
    "section_number": 123,
    "shelf_number": 12
}
```
Getting a list of Locations return value:
```
{
  "manufacturers": [
    {
        "href": "/api/locations/2/",
        "id": 2,
        "closet_name": "The Dark Lord's Closet",
        "section_number": 123,
        "shelf_number": 12
    },
    {
        "href": "/api/locations/5/",
        "id": 5,
        "closet_name": "Maddie's Closet",
        "section_number": 12,
        "shelf_number": 36
    }
  ]
}
```

### Bins:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List Bins | GET | http://localhost:8100/api/bins/
| Create a Bins | POST | http://localhost:8100/api/bins/ |
| Get a specific Bin's Details | GET | http://localhost:8100/api/bins/id/ |
| Update a specific Bin | PUT | http://localhost:8100/api/bins/id/ |
| Delete a specific Bin| DELETE | http://localhost:8100/api/bins/id/ |

JSON body to send data:

Create and Update a Location (SEND THIS JSON BODY):
```
{
  "name": "The Ringbearer's Bin",
  "bin_number": 1,
  "bin_size": 9
}
```
The return value of creating, viewing, updating a single Bin:
```
{
	"href": "/api/bins/2/",
	"id": 2,
	"bin_name": "The Ringbearer's Bin",
    "bin_number": 1,
    "bin_size": 9
}
```
Getting a list of Bins return value:
```
{
  "bins": [
    {
        "href": "/api/locations/2/",
        "id": 2,
        "closet_name": "The Ringbearer's Bin",
        "section_number": 1,
        "shelf_number": 9
    },
    {
        "href": "/api/locations/5/",
        "id": 5,
        "closet_name": "Samwise's Bin",
        "bin_number": 2,
        "shelf_number": 1
    }
  ]
}
```

## Shoes microservice

The Shoes microservice has 2 models: **BinVO** and **Shoe**.

The **BinVO** (value object) is populated by the **poller** and has an import_href as one of its properties, and name for other to provide clarity in testing purposes. The import_href is used by the **Shoe** model to create a shoe and associate it with the correct bin in the wardrobe.

The **Shoe** model has the following properties:
-manufacturer
-model
-color
-picture_url
-bin (A foreign key to the BinVO)

Each of these properties is required when creating a new shoe. The **Create a Shoe** API requires the inclusion of a specific id in the URL. This URL provides the location the Hat is stored in, and matches the bin's import_href.

## Hats microservice

The Hats microservice has 2 models: **LocationVO** and **Hat**.

The **LocationVO** (value object) is populated by the **poller** and has an import_href as one of its properties, and name for other to provide clarity in testing purposes. The import_href is used by the **Hat** model to create a hat and associate it with the correct location in the wardrobe.

The **Hat** model has the following properties:
- fabric
- style_name
- color
- picture_url
- location (A Foreign Key to the LocationVO)
Each of these properties is required when creating a new hat. The **Create a Hat** API requires the inclusion of a specific id in the URL. This URL provides the location the Hat is stored in, and matches the location's import_href.

## Accessing Endpoints to Send and View Data - Access through Insomnia:

### Hats API Calls:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List Hats | GET | http://localhost:8090/api/hats/
| Create a Hat | POST | http://localhost:8090/api/locations/id/hats/ |
| Get a specific Hats's Details | GET | http://localhost:8090/api/hats/id/ |
| Update a specific Hat | PUT | http://localhost:8090/api/hats/id/ |
| Delete a specific Hat | DELETE | http://localhost:8090/api/hats/id/ |

JSON body to send data:

Create and Update a Hat (SEND THIS JSON BODY):
```
{
  "fabric": "Straw",
  "style_name": "Farmer's Hat"
  "color": "Beige",
  "picture_url": "https://i.pinimg.com/736x/4d/13/a7/4d13a77fb621ce038157d0177ab692a5.jpg"
}
```
The return value of creating, viewing, updating a single Hat:
```
{
	"hat": {
		"id": 2,
		"fabric": "silk",
		"style_name": "Baseball Cap",
		"color": "Red",
		"picture_url": "https://www.pexels.com/photo/red-and-black-nike-fitted-cap-1124465/",
		"location": {
			"import_href": "/api/locations/2/",
			"name": "Giga Closet"
		}
	}
}
```
Getting a list of hats return value:
```
{
	"hats": [
		{
			"id": 3,
			"fabric": "Cloth",
			"style_name": "Baseball Cap",
			"color": "Red",
			"picture_url": "https://www.pexels.com/photo/red-and-black-nike-fitted-cap-1124465/",
			"location": {
				"import_href": "/api/locations/2/",
				"name": "Giga Closet"
			}
		},
		{
			"id": 4,
			"fabric": "Straw",
			"style_name": "Fedora",
			"color": "Beige",
			"picture_url": "https://www.pexels.com/photo/beige-and-black-hat-near-swimming-pool-984619/",
			"location": {
				"import_href": "/api/locations/5/",
				"name": "Maddie's Closet"
			}
		}
	]
}
```
### Shoes API Calls:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List Shoe | GET | http://localhost:8080/api/shoe/
| Create a Shoe | POST | http://localhost:8080/api/shoe/ |
| Get a specific Shoe's Details | GET | http://localhost:8080/api/shoe/id/ |
| Update a specific Shoe | PUT | http://localhost:8080/api/shoe/id/ |
| Delete a specific Shoe | DELETE | http://localhost:8080/api/shoe/id/ |

JSON body to send data:

Create and Update a Hat (SEND THIS JSON BODY):
```
{
  "manufacturer": "Nike",
  "model_name": "Jordan"
  "color": "Pink",
  "picture_url": "https://i.ebayimg.com/images/g/yA4AAOSwFglkNOe0/s-l1600.jpg"
}
```
The return value of creating, viewing, updating a single Shoe:
```
{
	"shoe": {
		"manufacturer": "Payless",
		"model_name": "Sandle",
		"color": "black",
		"picture_url": "",
		"bin": {
			"import_href": "/api/bins/3/",
			"closet_name": "cheese",
			"bin_number": 4,
			"bin_size": 3
	}
}
	}
}
```
Getting a list of Shoes return value:
```
{
	"shoes": [
		{
			"manufacturer": "bees",
			"model_name": "4",
			"color": "black",
			"picture_url": "",
			"id": 4,
			"bin": {
				"import_href": "/api/bins/1/",
				"closet_name": "Master",
				"bin_number": 2,
				"bin_size": 3
			}
		},
		{
			"manufacturer": "bees",
			"model_name": "4",
			"color": "black",
			"picture_url": "",
			"id": 5,
			"bin": {
				"import_href": "/api/bins/1/",
				"closet_name": "Master",
				"bin_number": 2,
				"bin_size": 3
			}

		}
	]
}
```
