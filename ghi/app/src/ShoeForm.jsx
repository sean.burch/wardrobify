import { useState, useEffect } from "react";

export default function ShoeForm() {
    const [bins, setBin] = useState([])
    const [formData, setFormData] = useState({
        manufacturer: "",
        model_name: "",
        color: "",
        picture_url: "",
        bin: ""
    })

    const handleSubmit = async (event) => {
        event.preventDefault()

        const binUrl = `http://localhost:8080/api/shoe/`

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json"
            }
        }
        const response = await fetch(binUrl, fetchConfig)
        if (response.ok) {
            const data = await response.json()
            console.log(data)

            setFormData({
                manufacturer: "",
                model_name: "",
                color: "",
                picture_url: "",
                bin: ""
            })
        }
    }

    const handleFormChange = (event) => {
        const value = event.target.value
        const name = event.target.name

        setFormData({
            ...formData,
            [name]: value
        })
    }

    const fetchData = async () => {
        const binUrl = "http://localhost:8100/api/bins/"
        const response = await fetch(binUrl)

        if (response.ok) {
            const data = await response.json()
            setBin(data.bins)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <form onSubmit={handleSubmit}>
                        <div className="text-center mb-3">
                            <span className="fs-3">Create a shoe</span>
                            </div>
                        <div className="form-floating mb-3">
                            <input type="text" onChange={handleFormChange} className="form-control" value={formData.manufacturer} name="manufacturer" id="manufactuer" placeholder="Manufacturer" />
                            <label htmlFor="fabric">Manufacturer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input type="text" onChange={handleFormChange} className="form-control" value={formData.model_name} name="model_name" id="model_name" placeholder="Model name" />
                            <label htmlFor="style_name">Model name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input type="text" onChange={handleFormChange} className="form-control" value={formData.color} name="color" id="color" placeholder="Color" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input type="text" onChange={handleFormChange} className="form-control" value={formData.picture_url} name="picture_url" id="picture_url" placeholder="Picture URL" />
                            <label htmlFor="picture_url">Picture URL</label>
                        </div>
                        <div className="mb-3">
                            <select value={formData.bin} onChange={handleFormChange} className="form-select" name="bin" id="bin">
                                <option defaultValue="">--Select a Bin--</option>
                                {bins.map(bin => {
                                    return (<option key={bin.id} value={bin.href}>{bin.bin_number}</option>)
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create!</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
