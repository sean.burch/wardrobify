import { useState, useEffect } from 'react';

export default function ShoeList() {

    const [shoes, setShoes] = useState([])

    const fetchData = async () => {
        const shoeurl = 'http://localhost:8080/api/shoe/'
        const response = await fetch(shoeurl)
        if (response.ok) {
            const data = await response.json()
            setShoes(data.shoes);
            console.log(data)
        }
    }

    const deleteShoe = async (id) => {
        const shoeurl = `http://localhost:8080/api/shoe/${id}/`
        const fetchConfig = {
            method: "delete"
        }
        const response = await fetch(shoeurl, fetchConfig)
        if (response.ok) {
            fetchData()
        }
    }

    useEffect(() => {
        fetchData();
    }, [])

    return (
        <div className="row">
            <div className="offset-0 col-12">
                <div className="shadow p-4 mt-4">
                    <div>
                        <table className="table">
                            <thead>
                                <tr>
                                    <th>Manufacturer</th>
                                    <th>Model Name</th>
                                    <th>Color</th>
                                    <th>Picture</th>
                                    <th>Bin</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                {shoes.map((shoe) => {
                                    return (
                                        <tr key={shoe.id}>
                                            <td>{shoe.manufacturer}</td>
                                            <td>{shoe.model_name}</td>
                                            <td>{shoe.color}</td>
                                            <td><img src={shoe.picture_url} style={{ width: "50px" }}/></td>
                                            <td>{shoe.bin.bin_number}</td>
                                            <td><button onClick={() => {deleteShoe(shoe.id)}} className="btn btn-danger">Delete</button></td>
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    )
}
